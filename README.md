# ChartProgressBar-Ohos

#### 项目介绍

- 项目名称：ChartProgressBar-Ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：绘制带有进度条样式的图表。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Releases 2.0.6

#### 效果演示

<img src="gif/chartprogress.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
```
allprojects {
   repositories {
       maven {
           url 'https://s01.oss.sonatype.org/content/repositories/releases/'
       }
   }
}
```
2.在entry模块的build.gradle文件中，
```
dependencies {
   implementation('com.gitee.chinasoft_ohos:ChartProgressBar-ohos_library:1.0.0')
   ......
}
```
在sdk6，DevEco Studio2.2 Beta1 下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在demo中通过布局调用自定义组件。

1、布局文件中引用自定义选择器界面效果：
```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    xmlns:app="http://schemas.huawei.com/apk/res-auto"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:orientation="vertical"
    ohos:background_element="#1665C1"
    >
    <DirectionalLayout
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:weight="1"
        />

    <com.hadiidbouk.charts.ChartProgressBar
        ohos:id="$+id:ChartProgressBar"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:weight="6"
        app:hdBarCanBeClick="true"
        app:hdBarHeight="170dp"
        app:hdBarWidth="7dp"
        app:hdBarRadius="10dp"
        app:hdMaxValue="10"
        app:hdEmptyColor="$color:empty"
        app:hdProgressColor="$color:progress"
        app:hdProgressClickColor="$color:progress_click"
        app:hdPinBackgroundColor="$color:pin_background"
        app:hdPinTextColor="$color:pin_text"
        app:hdPinPaddingBottom="5dp"
        app:hdBarTitleColor="$color:bar_title_color"
        app:hdBarTitleTxtSize="12sp"
        app:hdPinTxtSize="17sp"
        app:hdPinMarginBottom="45dp"
        app:hdPinMarginEnd="22dp"
        app:hdBarTitleMarginTop="9dp"
        app:hdPinDrawable="$media:ic_pin"
        app:hdProgressDisableColor="$color:progress_disable"
        app:hdBarTitleSelectedColor="$color:bar_title_selected_color"
        app:hdBarCanBeToggle="true"/>

    <DirectionalLayout
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:below="$id:ChartProgressBar"
        ohos:top_margin="40vp"
        ohos:alignment="bottom"
        ohos:orientation="horizontal"
        >
        <Button
            ohos:id="$+id:BtnClear"
            ohos:width="0vp"
            ohos:height="match_content"
            ohos:left_margin="5vp"
            ohos:weight="1"
            ohos:background_element="#ffffff"
            ohos:padding="10vp"
            ohos:text_size="15fp"
            ohos:text="CLEAR"/><!--ohos:onClick="BtnPressed"-->
        <Button
            ohos:id="$+id:BtnReset"
            ohos:width="0vp"
            ohos:height="match_content"
            ohos:left_margin="10vp"
            ohos:right_margin="10vp"
            ohos:weight="1"
            ohos:background_element="#ffffff"
            ohos:padding="10vp"
            ohos:text_size="15fp"
            ohos:text="RESET"/><!--ohos:onClick="BtnPressed"-->
        <Button
            ohos:id="$+id:BtnClearClick"
            ohos:width="0vp"
            ohos:height="match_content"
            ohos:right_margin="5vp"
            ohos:weight="1"
            ohos:background_element="#ffffff"
            ohos:padding="10vp"
            ohos:text_size="15fp"
            ohos:text="CLEAR CLICK"/><!--ohos:onClick="BtnPressed"-->
    </DirectionalLayout>

    <DirectionalLayout
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:weight="2"
        />

</DirectionalLayout>
```

其中 自定义进度条柱状图 是ChartProgressBar类：

```xml
<com.hadiidbouk.charts.ChartProgressBar
        ohos:id="$+id:ChartProgressBar"
        ohos:width="match_parent"
        ohos:height="match_content"
        ohos:weight="6"
        app:hdBarCanBeClick="true"
        app:hdBarHeight="170dp"
        app:hdBarWidth="7dp"
        app:hdBarRadius="10dp"
        app:hdMaxValue="10"
        app:hdEmptyColor="$color:empty"
        app:hdProgressColor="$color:progress"
        app:hdProgressClickColor="$color:progress_click"
        app:hdPinBackgroundColor="$color:pin_background"
        app:hdPinTextColor="$color:pin_text"
        app:hdPinPaddingBottom="5dp"
        app:hdBarTitleColor="$color:bar_title_color"
        app:hdBarTitleTxtSize="12sp"
        app:hdPinTxtSize="17sp"
        app:hdPinMarginBottom="45dp"
        app:hdPinMarginEnd="22dp"
        app:hdBarTitleMarginTop="9dp"
        app:hdPinDrawable="$media:ic_pin"
        app:hdProgressDisableColor="$color:progress_disable"
        app:hdBarTitleSelectedColor="$color:bar_title_selected_color"
        app:hdBarCanBeToggle="true"/>

```

2、在ability中通过点击事件，调用不同的接口逻辑
```java
      /**
     * onClick
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        int id = component.getId();
        switch (id) {
            case ResourceTable.Id_BtnClear:
                mChart.removeBarValues();
                break;
            case ResourceTable.Id_BtnReset:
                mChart.resetBarValues();
                break;
            case ResourceTable.Id_BtnClearClick:
                mChart.removeClickedBar();
                break;
            default:
        }
    }

```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0