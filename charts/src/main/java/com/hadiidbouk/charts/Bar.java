package com.hadiidbouk.charts;

import com.hadiidbouk.charts.util.Constant;
import ohos.agp.components.AttrSet;
import ohos.agp.components.ProgressBar;
import ohos.agp.render.Canvas;
import ohos.app.Context;

public class Bar extends ProgressBar {
    /**
     * 构造函数
     *
     * @param context context
     * @param attrs attrs
     */
    public Bar(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    /**
     * 构造函数
     *
     * @param context context
     */
    public Bar(Context context) {
        super(context);
    }

    /**
     * onDraw
     *
     * @param canvas canvas
     */
    protected synchronized void onDraw(Canvas canvas) {
        canvas.rotate(-Constant.NUMBER90);
        canvas.translate(-getHeight(), 0);
    }
}
