package com.hadiidbouk.charts;

import com.hadiidbouk.charts.util.Constant;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.ProgressBar;
import ohos.agp.utils.Color;

public class BarAnimation extends AnimatorProperty {
    private ProgressBar progressBar;
    private float from;
    private float to;

    /**
     * 构造函数
     *
     * @param progressBar progressBar
     * @param from from
     * @param to to
     */
    public BarAnimation(ProgressBar progressBar, float from, float to) {
        super();
        this.progressBar = progressBar;
        this.from = from;
        this.to = to;
    }

    /**
     * applyTransformation
     *
     * @param type type
     * @param number number
     * @param interpolatedTime interpolatedTime
     */
    protected void applyTransformation(int type, int number, float interpolatedTime) {
        float value = from + (to - from) * interpolatedTime;
        if (type == 0) {
            value = number;
        }
        progressBar.setWidth(Constant.NUMBER20);
        progressBar.setHeight((int) value);
        progressBar.setProgressWidth(Constant.NUMBER20);
        progressBar.setVisibility(Component.VISIBLE);
        progressBar.setIndeterminate(false);
        progressBar.setOrientation(DirectionalLayout.VERTICAL);
        progressBar.setStep(Constant.NUMBER10);
        progressBar.setMaxValue((int) value);
        progressBar.setProgressColor(new Color(Color.getIntColor("#e0e0e0")));
        progressBar.setProgressValue((int) value);
    }
}
