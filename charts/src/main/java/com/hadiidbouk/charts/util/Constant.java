/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hadiidbouk.charts.util;

/**
 * 常量工具类
 *
 * @since 2021-05-31
 */
public final class Constant {
    /**
     * SECOND
     */
    public static final int SECOND = 2;
    /**
     * THIRD
     */
    public static final int THIRD = 3;
    /**
     * NUMBER4
     */
    public static final int NUMBER4 = 4;
    /**
     * NUMBER5
     */
    public static final int NUMBER5 = 5;
    /**
     * STROKE_RATIO
     */
    public static final float STROKE_RATIO = 1.5f;
    /**
     * NUMBER8
     */
    public static final int NUMBER8 = 8;
    /**
     * SECONDF
     */
    public static final float SECONDF = 2f;
    /**
     * NUMBER35F
     */
    public static final float NUMBER35F = 3.5f;
    /**
     * SECONDFIVERF
     */
    public static final float SECONDFIVERF = 2.3f;
    /**
     * SECONDFIVERF
     */
    public static final float SECONDFIVERF1080 = 2.1f;
    /**
     * SECONDNORMAL
     */
    public static final float SECONDNORMAL = 2.45f;
    /**
     * SECONDNORMAL
     */
    public static final float SECONDNORMAL1080 = 2.25f;
    /**
     * ONESECONDF
     */
    public static final float ONESECONDF = 1.2f;
    /**
     * ONEEAT
     */
    public static final int ONEEAT = 180;
    /**
     * numner 1080
     */
    public static final int ONEZOREEATER = 1080;
    /**
     * NUMBER1000
     */
    public static final int NUMBER2000 = 2000;
    /**
     * FIVEZORE
     */
    public static final int FIVEZORE = 50;
    /**
     * NUMBER75
     */
    public static final int NUMBER75 = 75;
    /**
     * NUMBER77
     */
    public static final int NUMBER77 = 77;
    /**
     * NUMBER80
     */
    public static final int NUMBER80 = 80;
    /**
     * NUMBER100
     */
    public static final int NUMBER100 = 100;
    /**
     * NUMBER119
     */
    public static final int NUMBER119 = 119;
    /**
     * NUMBER150
     */
    public static final int NUMBER150 = 150;
    /**
     * NUMBER158
     */
    public static final int NUMBER158 = 158;
    /**
     * NUMBER200
     */
    public static final int NUMBER200 = 200;
    /**
     * NUMBER250
     */
    public static final int NUMBER250 = 250;
    /**
     * NUMBER380
     */
    public static final int NUMBER380 = 380;
    /**
     * NUMBER350
     */
    public static final int NUMBER350 = 350;
    /**
     * NUMBER595
     */
    public static final int NUMBER595 = 595;
    /**
     * NUMBER2211
     */
    public static final int NUMBER2211 = 2211;
    /**
     * NUMBER2143
     */
    public static final int NUMBER2143 = 2143;
    /**
     * NUMBER2085
     */
    public static final int NUMBER2085 = 2085;
    /**
     * NUMBER90
     */
    public static final int NUMBER90 = 90;
    /**
     * NUMBER40
     */
    public static final int NUMBER40 = 40;
    /**
     * NUMBER10
     */
    public static final int NUMBER10 = 10;
    /**
     * NUMBER15
     */
    public static final int NUMBER15 = 15;
    /**
     * NUMBER20
     */
    public static final int NUMBER20 = 20;
    /**
     * NUMBER30
     */
    public static final int NUMBER30 = 30;
    /**
     * NUMBER32
     */
    public static final int NUMBER32 = 32;
    /**
     * NUMBER33
     */
    public static final int NUMBER33 = 33;
    /**
     * TENF
     */
    public static final float TENF = 10.0f;
    /**
     * numner 0.5f
     */
    public static final float ZOREFIVEF = 0.5f;
    /**
     * NUMBER06F
     */
    public static final float NUMBER06F = 0.6f;
    /**
     * NUMBER08F
     */
    public static final float NUMBER08F = 0.8f;
    /**
     * NUMBER34F
     */
    public static final float NUMBER34F = 3.4f;
    /**
     * NUMBER80F
     */
    public static final float NUMBER80F = 8.0f;
    /**
     * NUMBER18F
     */
    public static final float NUMBER18F = 1.8f;
    /**
     * NUMBER72F
     */
    public static final float NUMBER72F = 7.2f;
    /**
     * NUMBER63F
     */
    public static final float NUMBER63F = 6.3f;
    /**
     * EAT
     */
    public static final int EAT = 8;
    /**
     * ONESIX
     */
    public static final int ONESIX = 16;
    /**
     * DEFAULT
     */
    public static final int DEFAULT = 0xff0000;
    /**
     * DEFAULT2
     */
    public static final int DEFAULT2 = 0x00ff00;
    /**
     * DEFAULT3
     */
    public static final int DEFAULT3 = 0x0000ff;
    /**
     * number 65536
     */
    public static final int NUMBER65536 = -65536;

    private Constant() {
    }
}
