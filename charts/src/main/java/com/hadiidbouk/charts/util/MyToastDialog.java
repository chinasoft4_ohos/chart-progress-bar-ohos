/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hadiidbouk.charts.util;

import com.hadiidbouk.charts.ResourceTable;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

/**
 * 吐司工具类
 *
 * @since 2021-05-31
 */
public class MyToastDialog extends ToastDialog {
    private Text textComponent;

    /**
     * 构造函数
     *
     * @param context context
     */
    public MyToastDialog(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        textComponent = new Text(context);
        int padding = Constant.NUMBER40;

        // 设置间距为10vp
        textComponent.setPadding(padding, padding, padding, padding);
        textComponent.setTextColor(Color.BLACK);
        textComponent.setTextAlignment(TextAlignment.CENTER);
        textComponent.setTextSize(Constant.NUMBER40);
        ShapeElement shapeElement = new ShapeElement(context, ResourceTable.Graphic_xtoast_frame);
        textComponent.setBackground(shapeElement);

        // 设置文字允许多行
        textComponent.setMultipleLine(true);

        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig();
        layoutConfig.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
        layoutConfig.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;

        layoutConfig.alignment = LayoutAlignment.CENTER;
        textComponent.setLayoutConfig(layoutConfig);
        setTransparent(true);
        setComponent(textComponent);

        setCornerRadius(AttrHelper.vp2px(ResourceTable.Float_xui_toast_radius, context));
    }

    @Override
    public MyToastDialog setText(String text) {
        textComponent.setText(text);
        return this;
    }
}