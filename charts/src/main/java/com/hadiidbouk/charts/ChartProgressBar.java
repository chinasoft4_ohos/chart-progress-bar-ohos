package com.hadiidbouk.charts;

import com.hadiidbouk.charts.util.ColorConverUtils;
import com.hadiidbouk.charts.util.Constant;
import com.hadiidbouk.charts.util.ResUtil;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import static ohos.agp.utils.LayoutAlignment.BOTTOM;
import static ohos.agp.utils.LayoutAlignment.CENTER;


public class ChartProgressBar extends StackLayout {
    private float mMaxValue;
    private int mBarWidth;
    private int mBarHeight;
    private int mEmptyColor;
    private int mProgressColor;
    private int mProgressClickColor;
    private int mBarRadius;
    private Context mContext;
    private int mPinTextColor;
    private int mPinBackgroundColor;
    private int mPinPaddingTop;
    private int mPinPaddingBottom;
    private int mPinPaddingEnd;
    private int mPinPaddingStart;
    private int mBarTitleColor;
    private float mBarTitleTxtSize;
    private float mPinTxtSize;
    private StackLayout oldFrameLayout;
    private boolean isBarCanBeClick;
    private ArrayList<BarData> mDataList;
    private boolean isOldBarClicked;
    private boolean isBarsEmpty;
    private int mPinMarginTop;
    private int mPinMarginBottom;
    private int mPinMarginEnd;
    private int mPinMarginStart;
    private int mPinDrawable;
    private ArrayList<Text> pins = new ArrayList<>();
    private int mBarTitleMarginTop;
    private int mBarTitleSelectedColor;
    private int mProgressDisableColor;
    private OnBarClickedListener listener;
    private boolean isBarCanBeToggle;
    private EventRunner eventRunner = EventRunner.create("MyEventRunner");
    private MyEventHandler myEventHandler;
    private int selectType = 0;
    private Component.ClickedListener barClickListener = new StackLayout.ClickedListener() {
        @Override
        public void onClick(Component view) {
            if (isBarsEmpty) {
                return;
            }
            selectType = 1;
            StackLayout frameLayout = (StackLayout) view;
            if (oldFrameLayout == frameLayout && isBarCanBeToggle) {
                if (isOldBarClicked) {
                    clickBarOff(frameLayout);
                } else {
                    clickBarOn(frameLayout);
                }
            } else {
                if (oldFrameLayout != null) {
                    clickBarOff(oldFrameLayout);
                }

                clickBarOn(frameLayout);
            }

            oldFrameLayout = frameLayout;

            if (listener != null) {
                listener.onBarClicked((int) frameLayout.getTag());
            }
        }
    };

    /**
     * 构造函数
     *
     * @param context context
     * @param attrs attrs
     */
    public ChartProgressBar(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context;
        selectType = 1;
        setAttrs(attrs, 0);
        myEventHandler = new MyEventHandler(eventRunner);
    }

    private void setAttrs(AttrSet attrs, int defStyleAttr) {
        mBarWidth = Constant.NUMBER40;
        mBarHeight = Constant.NUMBER595;
        mBarRadius = attrs.getAttr("hdBarRadius").get().getIntegerValue();

        // 颜色
        mEmptyColor = attrs.getAttr("hdEmptyColor").get().getIntegerValue();
        mProgressColor = attrs.getAttr("hdProgressColor").get().getIntegerValue();
        mProgressClickColor = attrs.getAttr("hdProgressClickColor").get().getIntegerValue();
        mProgressDisableColor = attrs.getAttr("hdProgressDisableColor").get().getIntegerValue();
        mBarTitleSelectedColor = attrs.getAttr("hdBarTitleSelectedColor").get().getIntegerValue();
        mPinTextColor = attrs.getAttr("hdPinTextColor").get().getIntegerValue();
        mPinBackgroundColor = attrs.getAttr("hdPinBackgroundColor").get().getIntegerValue();

        // -- 颜色
        mPinPaddingTop = Constant.NUMBER8;
        mPinPaddingBottom = Constant.NUMBER20;
        mPinPaddingEnd = Constant.NUMBER30;
        mPinPaddingStart = Constant.NUMBER30;
        isBarCanBeClick = attrs.getAttr("hdBarCanBeClick").get().getBoolValue();

        // 颜色
        mBarTitleColor = attrs.getAttr("hdBarTitleColor").get().getIntegerValue();

        // -- 颜色
        mMaxValue = attrs.getAttr("hdMaxValue").get().getFloatValue();
        mBarTitleTxtSize = Constant.NUMBER15;
        mPinTxtSize = attrs.getAttr("hdPinTxtSize").get().getIntegerValue();

        mPinMarginTop = 0;
        mPinMarginBottom = Constant.NUMBER158;
        mPinMarginEnd = Constant.NUMBER77;
        mPinMarginStart = 0;

        mBarTitleMarginTop = Constant.NUMBER32;
        mPinDrawable = attrs.getAttr("hdPinDrawable").get().getIntegerValue();
        isBarCanBeToggle = attrs.getAttr("hdBarCanBeToggle").get().getBoolValue();
    }

    public void setDataList(ArrayList<BarData> dataList) {
        mDataList = dataList;
    }

    public void setOnBarClickedListener(OnBarClickedListener lis) {
        this.listener = lis;
    }

    /**
     * 初始化界面数据
     */
    public void build() {
        removeAllComponents();
        DirectionalLayout linearLayout = new DirectionalLayout(mContext);

        int hh = Constant.NUMBER119;
        if (mPinMarginBottom != 0) {
            hh += mPinMarginBottom / Constant.SECOND;
        }
        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                LayoutConfig.MATCH_PARENT,
                LayoutConfig.MATCH_PARENT
        );
        linearLayout.setPadding(0, hh, 0, 0);
        linearLayout.setOrientation(DirectionalLayout.HORIZONTAL);
        linearLayout.setLayoutConfig(params);

        addComponent(linearLayout);
        int ii = 0;
        for (BarData data : mDataList) {
            int barValue = (int) (data.getBarValue() * Constant.NUMBER100);
            StackLayout bar = getBar(data.getBarTitle(), barValue, ii);
            linearLayout.addComponent(bar);
            ii++;
        }

        setPins();
    }

    private StackLayout getBar(final String title, final int value, final int index) {
        int maxValue = (int) (mMaxValue * Constant.NUMBER100);
        DirectionalLayout linearLayout = new DirectionalLayout(mContext);
        StackLayout.LayoutConfig params = new StackLayout.LayoutConfig(
                LayoutConfig.MATCH_PARENT,
                LayoutConfig.MATCH_PARENT
        );

        params.alignment = CENTER;
        linearLayout.setLayoutConfig(params);
        linearLayout.setOrientation(DirectionalLayout.VERTICAL);
        linearLayout.setAlignment(BOTTOM | CENTER);

        // Adding bar
        Bar bar = new Bar(mContext, null);
        bar.setWidth(Constant.NUMBER20);
        bar.setHeight(value);
        bar.setProgressWidth(Constant.NUMBER20);
        bar.setVisibility(Component.VISIBLE);
        bar.setIndeterminate(false);
        bar.setOrientation(DirectionalLayout.VERTICAL);
        bar.setMaxValue(value);
        bar.setProgressColor(new Color(Color.getIntColor("#e0e0e0")));
        bar.setProgressValue(value);

        BarAnimation anim = new BarAnimation(bar, 0, value);
        linearLayout.addComponent(bar);

        // Adding txt below bar
        Text txtBar = new Text(mContext);

        LayoutConfig txtParams = new LayoutConfig(
                LayoutConfig.MATCH_CONTENT,
                LayoutConfig.MATCH_CONTENT
        );
        txtBar.setTextSize(Constant.NUMBER33);
        txtBar.setText(title);
        txtBar.setTextAlignment(0);

        txtBar.setTextColor(new Color(Color.getIntColor("#B6BDD5")));
        txtBar.setPadding(0, mBarTitleMarginTop, 0, 0);

        txtBar.setLayoutConfig(txtParams);

        linearLayout.addComponent(txtBar);

        StackLayout rootFrameLayout = new StackLayout(mContext);
        DirectionalLayout.LayoutConfig rootParams =
                new DirectionalLayout.LayoutConfig(LayoutConfig.MATCH_PARENT,
                        LayoutConfig.MATCH_PARENT, 0, 1f);
        rootParams.alignment = CENTER;
        rootFrameLayout.setLayoutConfig(rootParams);

        // Adding bar + title
        rootFrameLayout.addComponent(linearLayout);
        if (isBarCanBeClick) {
            rootFrameLayout.setClickedListener(barClickListener);
        }
        rootFrameLayout.setTag(index);
        return rootFrameLayout;
    }

    private void setPins() {
        pins.clear();
        int maxValue = (int) (mMaxValue * Constant.NUMBER100);
        int childCount = getChildCount();
        DirectionalLayout linearLayout = null;
        for (int ii = 0; ii < childCount; ii++) {
            Component view = getComponentAt(ii);

            if (view instanceof DirectionalLayout) {
                linearLayout = (DirectionalLayout) view;
                break;
            }
        }
        if (linearLayout != null) {
            childCount = linearLayout.getChildCount();
            for (int iii = 0; iii < childCount; iii++) {
                Component view = linearLayout.getComponentAt(iii);
                BarData data = mDataList.get(iii);
                int value = (int) (data.getBarValue() * Constant.NUMBER100);
                String pinTxt = data.getPinText();
                StackLayout barFrame = (StackLayout) view;
                int frameCount = barFrame.getChildCount();
                for (int jj = 0; jj < frameCount; jj++) {
                    Component vv = barFrame.getComponentAt(jj);
                    if (vv instanceof DirectionalLayout) {
                        int count = ((DirectionalLayout) vv).getChildCount();
                        for (int kk = 0; kk < count; kk++) {
                            if (((DirectionalLayout) vv).getComponentAt(kk) instanceof Bar) {
                                Text pinTxtView = new Text(mContext);
                                pinTxtView.setBackground(ResUtil.getPixelMapDrawable(getContext(),
                                        ResourceTable.Media_ic_pin));
                                pinTxtView.setPadding(mPinPaddingStart, mPinPaddingTop, mPinPaddingEnd,
                                        mPinPaddingBottom);
                                pinTxtView.setTextColor(new Color(Color.getIntColor("#ffffff")));
                                pinTxtView.setText(pinTxt);
                                pinTxtView.setMaxTextLines(1);
                                pinTxtView.setTextSize(Constant.FIVEZORE);
                                pinTxtView.setTextAlignment(0);
                                Rect bounds = new Rect();
                                Paint textPaint = new Paint();
                                textPaint.getTextBounds(pinTxt);
                                int pinBackgroundHeight = bounds.getHeight();
                                int pinBackgroundWidth = bounds.getWidth();
                                int xx = (int) (view.getContentPositionX() - pinBackgroundWidth / Constant.SECOND
                                        + view.getWidth() / Constant.SECOND);
                                int yy = (int) (view.getContentPositionY());
                                StackLayout.LayoutConfig valueParams = new StackLayout.LayoutConfig(
                                        LayoutConfig.MATCH_CONTENT,
                                        LayoutConfig.MATCH_CONTENT
                                );
                                Bar bar = (Bar) ((DirectionalLayout) vv).getComponentAt(kk);
                                int pinPosition = mBarHeight - ((value * mBarHeight) / maxValue) - pinBackgroundHeight
                                        / Constant.SECOND;
                                pinTxtView.setLayoutConfig(valueParams);

                                Optional<Display>
                                        display = DisplayManager.getInstance().getDefaultDisplay(mContext);
                                DisplayAttributes displayAttributes = display.get().getAttributes();

                                pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop);
                                if (iii == 0) {
                                    pinTxtView.setTranslationX(displayAttributes.width / Constant.FIVEZORE
                                            - Constant.NUMBER5);
                                    if (displayAttributes.height == Constant.NUMBER2211) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER200);
                                    } else if (displayAttributes.height == Constant.NUMBER2143) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER100);
                                    } else if (displayAttributes.height == Constant.NUMBER2085) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER150);
                                    } else {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER350);
                                    }
                                } else if (iii == 1) {
                                    pinTxtView.setTranslationX(displayAttributes.width / Constant.NUMBER4
                                            - Constant.NUMBER75);
                                    if (displayAttributes.height == Constant.NUMBER2211) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop);
                                    } else if (displayAttributes.height == Constant.NUMBER2143) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                - Constant.NUMBER100);
                                    } else if (displayAttributes.height == Constant.NUMBER2085) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                - Constant.NUMBER30);
                                    } else {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER150);
                                    }
                                } else if (iii == Constant.SECOND) {
                                    pinTxtView.setTranslationX(displayAttributes.width / Constant.THIRD
                                            + Constant.NUMBER20);
                                    if (displayAttributes.height == Constant.NUMBER2211) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER250);
                                    } else if (displayAttributes.height == Constant.NUMBER2143) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER150);
                                    } else if (displayAttributes.height == Constant.NUMBER2085) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER200);
                                    } else {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER380);
                                    }
                                } else if (iii == Constant.THIRD) {
                                    pinTxtView.setTranslationX(displayAttributes.width / Constant.SECOND
                                            + Constant.NUMBER20);
                                    if (displayAttributes.height == Constant.NUMBER2211) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.FIVEZORE);
                                    } else if (displayAttributes.height == Constant.NUMBER2143) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                - Constant.FIVEZORE);
                                    } else if (displayAttributes.height == Constant.NUMBER2085) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop);
                                    } else {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER150);
                                    }
                                } else if (iii == Constant.NUMBER4) {
                                    pinTxtView.setTranslationX(displayAttributes.width * Constant.THIRD
                                            / Constant.NUMBER4 - Constant.NUMBER80);
                                    if (displayAttributes.height == Constant.NUMBER2211) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.FIVEZORE);
                                    } else if (displayAttributes.height == Constant.NUMBER2143) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                - Constant.FIVEZORE);
                                    } else if (displayAttributes.height == Constant.NUMBER2085) {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.FIVEZORE);
                                    } else {
                                        pinTxtView.setTranslationY(yy + pinPosition + mPinMarginTop
                                                + Constant.NUMBER200);
                                    }
                                }

                                addComponent(pinTxtView);
                                pins.add(pinTxtView);
                                pinTxtView.setVisibility(Component.INVISIBLE);
                                pinTxtView.setTag(iii);
                            }
                        }
                    }
                }
            }
        }
    }

    public void setMaxValue(float value) {
        this.mMaxValue = value;
    }

    private float getSp(float size) {
        return size / Constant.NUMBER35F;
    }

    public boolean isBarsEmpty() {
        return isBarsEmpty;
    }

    private void clickBarOn(StackLayout frameLayout) {
        pins.get((int) frameLayout.getTag()).setVisibility(Component.VISIBLE);
        isOldBarClicked = true;
        int childCount = frameLayout.getChildCount();
        for (int ii = 0; ii < childCount; ii++) {
            Component childView = frameLayout.getComponentAt(ii);
            if (childView instanceof DirectionalLayout) {
                DirectionalLayout linearLayout = (DirectionalLayout) childView;
                Bar bar = (Bar) linearLayout.getComponentAt(0);
                Text titleTxtView = (Text) linearLayout.getComponentAt(1);
                if (mPinBackgroundColor != 0) {
                    bar.setProgressColor(new Color(Color.getIntColor("#F2912C")));
                } else {
                    bar.setProgressColor(new Color(Color.getIntColor("#F2912C")));
                }

                if (mBarTitleSelectedColor > 0) {
                    int[] rgb = ColorConverUtils.colorToRgb(mBarTitleSelectedColor);
                    titleTxtView.setTextColor(new Color(Color.rgb(rgb[0], rgb[1], rgb[Constant.SECOND])));
                } else {
                    titleTxtView.setTextColor(new Color(Color.getIntColor("#ffffff")));
                }
            }
        }
    }

    private void clickBarOff(StackLayout frameLayout) {
        pins.get((int) frameLayout.getTag()).setVisibility(Component.INVISIBLE);

        isOldBarClicked = false;

        int childCount = frameLayout.getChildCount();
        for (int ii = 0; ii < childCount; ii++) {
            Component childView = frameLayout.getComponentAt(ii);
            if (childView instanceof DirectionalLayout) {
                DirectionalLayout linearLayout = (DirectionalLayout) childView;
                Bar bar = (Bar) linearLayout.getComponentAt(0);
                Text titleTxtView = (Text) linearLayout.getComponentAt(1);
                bar.setProgressColor(new Color(Color.getIntColor("#e0e0e0")));
                titleTxtView.setTextColor(new Color(Color.getIntColor("#B6BDD5")));
            }
        }
    }

    public ArrayList<BarData> getData() {
        return mDataList;
    }

    /**
     * clear功能
     */
    public void removeBarValues() {
        if (selectType == 1) {
            removeBarMinValues();
            return;
        }
        if (oldFrameLayout != null) {
            removeClickedBar(0);
        }

        final int barsCount = ((DirectionalLayout) this.getComponentAt(0)).getChildCount();
        final int[] type2 = {0};
        for (int ii = 0; ii < barsCount; ii++) {
            final int numberid = ii;
            final int[] type = {0};
            StackLayout rootFrame = (StackLayout) ((DirectionalLayout) this.getComponentAt(0)).getComponentAt(ii);
            int rootChildCount = rootFrame.getChildCount();
            for (int jj = 0; jj < rootChildCount; jj++) {
                Component childView = rootFrame.getComponentAt(jj);
                if (childView instanceof DirectionalLayout) {
                    // bar
                    DirectionalLayout barContainerLinear = (DirectionalLayout) childView;
                    int barContainerCount = barContainerLinear.getChildCount();
                    for (int kk = 0; kk < barContainerCount; kk++) {
                        Component view = barContainerLinear.getComponentAt(kk);
                        if (view instanceof Bar) {
                            BarAnimation anim = new BarAnimation((Bar) view, 0, (int) (
                                    mDataList.get(ii).getBarValue() * Constant.NUMBER100));
                            int num = (int) (mDataList.get(ii).getBarValue() * Constant.NUMBER100);
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {
                                int value = 0;
                                @Override
                                public void run() {
                                    if (selectType == 0) {
                                        InnerEvent normalInnerEvent = InnerEvent.get(1, value, anim);
                                        myEventHandler.sendEvent(normalInnerEvent, 0, EventHandler.Priority.IMMEDIATE);
                                        if (type2[0] == 0) {
                                            if (numberid == 1 || numberid == Constant.THIRD || numberid
                                                    == Constant.NUMBER4) {
                                                value += Constant.NUMBER20;
                                            } else if (numberid == Constant.SECOND) {
                                                value += Constant.NUMBER5;
                                            } else {
                                                value += Constant.NUMBER10;
                                            }
                                            if (value >= num) {
                                                type2[0] = 1;
                                                InnerEvent normalInnerEvent2 = InnerEvent.get(1, 0, anim);
                                                myEventHandler.sendEvent(normalInnerEvent2, 0, EventHandler
                                                        .Priority.IMMEDIATE);
                                            }
                                        } else if (type2[0] == 1) {
                                            if (numberid == 1 || numberid == Constant.THIRD || numberid
                                                    == Constant.NUMBER4) {
                                                value -= Constant.NUMBER20;
                                            } else if (numberid == Constant.SECOND) {
                                                value -= Constant.NUMBER5;
                                            } else {
                                                value -= Constant.NUMBER10;
                                            }

                                            if (value <= 0) {
                                                InnerEvent normalInnerEvent2 = InnerEvent.get(1, 0, anim);
                                                myEventHandler.sendEvent(normalInnerEvent2, 0,
                                                        EventHandler.Priority.IMMEDIATE);
                                                timer.cancel();
                                                return;
                                            }
                                        }
                                    }
                                }
                            }, 0, Constant.NUMBER8);
                        }
                    }
                }
            }
        }
        isBarsEmpty = true;
    }

    /**
     * 点击充值后的下降操作
     */
    public void removeBarMinValues() {
        if (oldFrameLayout != null) {
            removeClickedBar(0);
        }
        final int barsCount = ((DirectionalLayout) this.getComponentAt(0)).getChildCount();
        for (int ii = 0; ii < barsCount; ii++) {
            final int numberid = ii;
            final int[] type = {0};
            StackLayout rootFrame = (StackLayout) ((DirectionalLayout) this.getComponentAt(0)).getComponentAt(ii);
            int rootChildCount = rootFrame.getChildCount();
            for (int jj = 0; jj < rootChildCount; jj++) {
                Component childView = rootFrame.getComponentAt(jj);
                if (childView instanceof DirectionalLayout) {
                    // bar
                    DirectionalLayout barContainerLinear = (DirectionalLayout) childView;
                    int barContainerCount = barContainerLinear.getChildCount();
                    for (int kk = 0; kk < barContainerCount; kk++) {
                        Component view = barContainerLinear.getComponentAt(kk);
                        if (view instanceof Bar) {
                            BarAnimation anim = new BarAnimation((Bar) view, 0, (int) (
                                    mDataList.get(ii).getBarValue() * Constant.NUMBER100));
                            int num = (int) (mDataList.get(ii).getBarValue() * Constant.NUMBER100);
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {
                                int value = 0;
                                @Override
                                public void run() {
                                    // selectType为1的时候表示做减少直到0
                                    if (type[0] == 0) {
                                        value = num;
                                        type[0] = 1;
                                    }
                                    InnerEvent normalInnerEvent2 = InnerEvent.get(Constant.SECOND, value, anim);
                                    myEventHandler.sendEvent(normalInnerEvent2, 0, EventHandler.Priority.IMMEDIATE);
                                    if (numberid == 1 || numberid == Constant.THIRD || numberid
                                            == Constant.NUMBER4) {
                                        value -= Constant.NUMBER20;
                                    } else if (numberid == Constant.SECOND) {
                                        value -= Constant.NUMBER5;
                                    } else {
                                        value -= Constant.NUMBER10;
                                    }
                                    if (value <= 0) {
                                        InnerEvent normalInnerEvent3 = InnerEvent.get(Constant.SECOND, 0, anim);
                                        myEventHandler.sendEvent(normalInnerEvent3, 0,
                                                EventHandler.Priority.IMMEDIATE);
                                        timer.cancel();
                                    }
                                }
                            }, 0, Constant.NUMBER8);
                        }
                    }
                }
            }
        }
        isBarsEmpty = true;
    }

    /**
     * MyEventHandler
     *
     * @author: ChartProgressBar-ohos
     * @since 2021-05-31
     */
    private class MyEventHandler extends EventHandler {
        /**
         * 构造函数
         *
         * @param runner runner
         * @throws IllegalArgumentException IllegalArgumentException
         */
        protected MyEventHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        public void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (event == null) {
                return;
            }
            int eventId = event.eventId;
            switch (eventId) {
                case 1:
                    // 待执行的操作，由开发者定义
                    int value = (int) event.param;
                    BarAnimation anim = (BarAnimation) event.object;
                    mContext.getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            anim.applyTransformation(0, value, 0);
                        }
                    }, Constant.NUMBER10);
                    break;
                case Constant.SECOND:
                    // 待执行的操作，由开发者定义
                    int value2 = (int) event.param;
                    BarAnimation anim2 = (BarAnimation) event.object;
                    mContext.getUITaskDispatcher().delayDispatch(new Runnable() {
                        @Override
                        public void run() {
                            anim2.applyTransformation(0, value2, 0);
                        }
                    }, Constant.NUMBER10);
                    if (value2 == 0) {
                        selectType = 0;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * removeClickedBar
     *
     * @param status status
     */
    public void removeClickedBar(int status) {
        if (null == oldFrameLayout) {
            return;
        }
        if (status != 0) {
            selectType = 0;
        }
        clickBarOff(oldFrameLayout);
    }

    /**
     * resetBarValues
     */
    public void resetBarValues() {
        selectType = 1;
        if (oldFrameLayout != null) {
            removeClickedBar(0);
        }

        final int barsCount = ((DirectionalLayout) this.getComponentAt(0)).getChildCount();

        for (int ii = 0; ii < barsCount; ii++) {
            final int numberid = ii;
            StackLayout rootFrame = (StackLayout) ((DirectionalLayout) this.getComponentAt(0)).getComponentAt(ii);
            int rootChildCount = rootFrame.getChildCount();
            for (int jj = 0; jj < rootChildCount; jj++) {
                Component childView = rootFrame.getComponentAt(jj);
                if (childView instanceof DirectionalLayout) {
                    // bar
                    DirectionalLayout barContainerLinear = (DirectionalLayout) childView;
                    int barContainerCount = barContainerLinear.getChildCount();
                    for (int kk = 0; kk < barContainerCount; kk++) {
                        Component view = barContainerLinear.getComponentAt(kk);
                        if (view instanceof Bar) {
                            BarAnimation anim = new BarAnimation((Bar) view, (int) (mDataList.get(ii).getBarValue()
                                    * Constant.NUMBER100), 0);
                            int num = (int) (mDataList.get(ii).getBarValue() * Constant.NUMBER100);
                            Timer timer = new Timer();
                            timer.schedule(new TimerTask() {
                                int value = 0;

                                @Override
                                public void run() {
                                    if (numberid == 1 || numberid == Constant.THIRD || numberid == Constant.NUMBER4) {
                                        value += Constant.NUMBER20;
                                    } else {
                                        value += Constant.NUMBER10;
                                    }
                                    InnerEvent normalInnerEvent = InnerEvent.get(1, value, anim);
                                    myEventHandler.sendEvent(normalInnerEvent, 0, EventHandler.Priority.IMMEDIATE);

                                    if (value >= num) {
                                        timer.cancel();
                                        return;
                                    }
                                }
                            }, 0, Constant.NUMBER8);
                        }
                    }
                }
            }
        }
        isBarsEmpty = false;
    }

    /**
     * disableBar
     *
     * @param index index
     */
    public void disableBar(int index) {
        final int barsCount = ((DirectionalLayout) this.getComponentAt(0)).getChildCount();

        for (int ii = 0; ii < barsCount; ii++) {
            StackLayout rootFrame = (StackLayout) ((DirectionalLayout) this.getComponentAt(0)).getComponentAt(ii);
            int rootChildCount = rootFrame.getChildCount();
            for (int jj = 0; jj < rootChildCount; jj++) {
                if ((int) rootFrame.getTag() != index) {
                    continue;
                }
                rootFrame.setEnabled(false);
                rootFrame.setClickable(false);
                Component childView = rootFrame.getComponentAt(jj);
                if (childView instanceof DirectionalLayout) {
                    // bar
                    DirectionalLayout barContainerLinear = (DirectionalLayout) childView;
                    int barContainerCount = barContainerLinear.getChildCount();
                    for (int kk = 0; kk < barContainerCount; kk++) {
                        Component view = barContainerLinear.getComponentAt(kk);
                        if (view instanceof Bar) {
                            Bar bar = (Bar) view;
                            if (mProgressDisableColor > 0) {
                                int[] rgb = ColorConverUtils.colorToRgb(mProgressDisableColor);
                                bar.setProgressColor(new Color(Color.rgb(rgb[0], rgb[1], rgb[Constant.SECOND])));
                            } else {
                                bar.setProgressColor(new Color(Color.getIntColor("#aaaaaa")));
                                bar.setAlpha(Constant.NUMBER06F);
                            }
                        } else {
                            Text titleTxtView = (Text) view;
                            if (mProgressDisableColor > 0) {
                                int[] rgb = ColorConverUtils.colorToRgb(mProgressDisableColor);
                                titleTxtView.setTextColor(new Color(Color.rgb(rgb[0], rgb[1], rgb[Constant.SECOND])));
                            } else {
                                titleTxtView.setTextColor(new Color(Color.getIntColor("#aaaaaa")));
                                titleTxtView.setAlpha(Constant.NUMBER08F);
                            }
                        }
                    }
                }
            }
        }
    }
}
