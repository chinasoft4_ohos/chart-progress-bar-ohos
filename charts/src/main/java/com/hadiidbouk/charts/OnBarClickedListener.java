package com.hadiidbouk.charts;

public interface OnBarClickedListener {
    /**
     * 接口函数
     *
     * @param index index
     */
    void onBarClicked(int index);
}
