/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hadiidbouk.chartprogressbar;

import com.hadiidbouk.charts.BarData;
import com.hadiidbouk.charts.ChartProgressBar;
import com.hadiidbouk.charts.OnBarClickedListener;
import com.hadiidbouk.charts.util.Constant;
import com.hadiidbouk.charts.util.MyToastDialog;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import java.util.ArrayList;

/**
 * 主入口
 *
 * @since 2021-05-31
 */
public class MainAbility extends Ability implements OnBarClickedListener, Component.ClickedListener {
    private ChartProgressBar mChart;
    private Text msgToast;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        ArrayList<BarData> dataList = new ArrayList<BarData>();

        BarData data = new BarData("Sep", Constant.NUMBER34F, "3.4€");
        dataList.add(data);

        data = new BarData("Oct", Constant.NUMBER80F, "8.0€");
        dataList.add(data);

        data = new BarData("Nov", Constant.NUMBER18F, "1.8€");
        dataList.add(data);

        data = new BarData("Dec", Constant.NUMBER72F, "7.3€");
        dataList.add(data);

        data = new BarData("Jan", Constant.NUMBER63F, "6.2€");
        dataList.add(data);

        data = new BarData("Feb", Constant.NUMBER34F, "3.3€");
        dataList.add(data);

        mChart = (ChartProgressBar) findComponentById(ResourceTable.Id_ChartProgressBar);

        mChart.setDataList(dataList);
        mChart.build();
        mChart.setOnBarClickedListener(this);
        mChart.disableBar(dataList.size() - 1);

        Button btnClear = (Button)findComponentById(ResourceTable.Id_BtnClear);
        Button btnReset = (Button)findComponentById(ResourceTable.Id_BtnReset);
        Button btnClearClick = (Button)findComponentById(ResourceTable.Id_BtnClearClick);
        btnClear.setClickedListener(this);
        btnReset.setClickedListener(this);
        btnClearClick.setClickedListener(this);
        msgToast = (Text)findComponentById(ResourceTable.Id_msg_toast1);
    }

    /**
     * onClick
     *
     * @param component component
     */
    @Override
    public void onClick(Component component) {
        int id = component.getId();
        switch (id) {
            case ResourceTable.Id_BtnClear:
                mChart.removeBarValues();
                break;
            case ResourceTable.Id_BtnReset:
                mChart.resetBarValues();
                break;
            case ResourceTable.Id_BtnClearClick:
                mChart.removeClickedBar(0);
                break;
            default:
        }
    }

    /**
     * onBarClicked
     *
     * @param index index
     */
    @Override
    public void onBarClicked(int index) {
        msgToast.setText(String.valueOf(index));
        msgToast.setVisibility(Component.VISIBLE);
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                msgToast.setVisibility(Component.HIDE);
            }
        },Constant.NUMBER2000);
    }

    /**
     * show
     *
     * @param message message
     */
    private void show(String message) {
        MyToastDialog toastDialog = (MyToastDialog) new MyToastDialog(getContext())
                .setAlignment(LayoutAlignment.BOTTOM)
                .setOffset(0, Constant.NUMBER100);
        toastDialog.setText(message);
        toastDialog.show();
    }
}
